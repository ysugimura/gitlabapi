package gitlabapi;

import java.io.*;
import java.util.*;

import com.google.gson.reflect.*;
import com.gwtcenter.jsonMapper.*;
import com.squareup.okhttp.*;

import gitlabapi.Data.*;

public class Api {

  /** 全プロジェクトをリストする */
  public static List<Project> list(OkHttpClient client, String accessToken) throws IOException {
    Serializer<List<Project>> projectListSerializer = new Serializer<>(new TypeToken<List<Project>>() {});
    List<Project>projectList = new ArrayList<>();
    
    for (int page = 1; ; page++) {
      Request request = new Request.Builder()
        .url("https://gitlab.com/api/v4/projects?per_page=20&page=" + page + "&simple=yes&owned=yes&private_token=" + accessToken)
        .get()
        .build();    
      Response response = client.newCall(request).execute();
      String result = response.body().string();        
      List<Project>list = projectListSerializer.deserialize(result);
      if (list.size() == 0) break;
      projectList.addAll(list);
    }    
    return projectList;
  }
}

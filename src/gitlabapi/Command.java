package gitlabapi;

public class Command {

  
  static void exec(String[]command) throws Exception {
    ProcessBuilder builder = new ProcessBuilder();
    builder.command(command);
    Process p = builder.start();
    new ProcessOutput(p) {
      protected void stderr(String line) {
        System.out.println(line);
      }
      protected void stdout(String line) {
        System.out.println(line);        
      }      
    };    
    p.waitFor();
  }

}

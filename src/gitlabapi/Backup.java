package gitlabapi;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

import com.squareup.okhttp.*;

import gitlabapi.Data.*;

public class Backup {

  
  /** gitリポジトリを作成するフォルダ */
  Path baseDir;
  
  /** ユーザ名 */
  String user;
  
  /** パスワード */
  String password;
  
  /** アクセストークン */
  String accessToken;

  /** baseDir直下のフォルダ一覧 */
  private Set<String>gitDirs;
  
  void execute(List<String>argList) throws Exception {
    getArguments(argList);    
    getDirectories();
    List<ProjectPath>list = getProjectPaths();    
    for (ProjectPath p : list) {
      cloneOrUpdate(p);
    }
    for (String gitDir : gitDirs) {
      NioFiles.delete(baseDir.resolve(gitDir));
    }
  }
  
  /** 引数を取得する */
  void getArguments(List<String>argList) {
    baseDir = Paths.get(argList.remove(0));
    user = argList.remove(0);
    password = argList.remove(0);
    accessToken = argList.remove(0);
  }

  /** ベースディレクトリ直下のフォルダ名称を取得する */
  void getDirectories() throws IOException {
    NioFiles.mkdirs(baseDir);    
    gitDirs = Files.list(baseDir).filter(p-> {
      if (Files.isRegularFile(p)) { 
        try {
          NioFiles.delete(p);
        } catch (IOException ex) {}
        return false;
      }
      return true;
    }).map(p->p.getFileName().toString()).collect(Collectors.toSet());    
  }

  /** GitLabから全プロジェクトをリストアップし、{@link ProjectPath}リストにする 
   * @throws IOException */
  List<ProjectPath>getProjectPaths() throws IOException {
    OkHttpClient client = new OkHttpClient();
    List<Project> projects = Api.list(client, accessToken);
    List<ProjectPath> list = projects.stream().map(p -> new ProjectPath(p.namespace.path, p.path))
        .collect(Collectors.toList());    
    return list;
  }
  
  /** グループ名・プロジェクト名を指定してリポジトリを作成あるいは更新する */
  void cloneOrUpdate(ProjectPath path) throws Exception {
    boolean r = gitDirs.remove(path.dirName());
    if (!r) {
      clone(path);
    } else {
      update(path);
    }
  }

  /** クローンする */
  void clone(ProjectPath path) throws Exception {
    Command.exec(new String[] { "git", "clone", "--mirror",
        "https://" + user + ":" + password + "@gitlab.com/" + path.group + "/" + path.project + ".git",
        path.getPath(baseDir).toString() });
  }

  /** 更新する */
  void update(ProjectPath path) throws Exception {
    Command.exec(new String[] { "git", "--git-dir=" + path.getPath(baseDir).toString(), "fetch", "--all" });
  }
}

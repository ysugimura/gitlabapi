package gitlabapi;

import java.nio.file.*;

public class Data {

  
  public static class Project {
    /** プロジェクトID */
    public String id;
    
    /** プロジェクトの名称 */
    public String name;;
    
    /** URLに現れるパス */
    public String path;
    
    /** ネームスペース */
    public Namespace namespace;
    
    @Override
    public String toString() {
      return id + "," + path + "," + namespace.path;
    }
  }
  
  public static class Namespace {
    public String path;
  }
  
  public static class ProjectPath {
    public String group;
    public String project;  
    public ProjectPath(String group, String project) {
      this.group = group;
      this.project = project;
    }
    public String dirName() {
      return group + "." + project + ".git";      
    }
    public Path getPath(Path baseDir) {
      return baseDir.resolve(dirName());
    }
  }
}

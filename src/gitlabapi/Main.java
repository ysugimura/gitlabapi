package gitlabapi;

import java.io.*;
import java.util.*;
import java.util.stream.*;

public class Main {

  /** 
   * 
   * @param args
   * @throws Exception
   */
  public static void main(String[]args) throws Exception {  
    List<String>argList = Arrays.stream(args).collect(Collectors.toList());
    String cmd = argList.remove(0);    
    if (cmd.equals("backup")) new Backup().execute(argList);
    else throw new IOException("" + cmd + " not supported");
  }
}

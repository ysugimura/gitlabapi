package gitlabapi;

import java.io.*;
import java.util.*;

import com.squareup.okhttp.*;

import gitlabapi.Data.*;

public class ApiTest {

  public static void main(String[]args) throws IOException {
    List<Project>projects = Api.list(new OkHttpClient(), args[0]);
    projects.forEach(System.out::println);
    System.out.println("" + projects.size());
  }
}
